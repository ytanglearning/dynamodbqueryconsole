﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using DynamoDBQueryConsole.Model;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;

namespace DynamoDBQueryConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var dbname = "Customer";
            var dClient = new AmazonDynamoDBClient("AKIA2SRTTKFXLWAKV47N", "DJsuJg22RtYkirkvxKFo0p6+Mw9BOiPpAJY5kXcD", RegionEndpoint.USEast2);

            try
            {
                var CustomerById = Task.Run(() => GetCustomerById(dbname, dClient)).GetAwaiter().GetResult();
                CustomerById.ForEach(x => Debug.WriteLine(x.FirstName + " " + x.LastName + " " + x.Region));

                var CustomersByRegion = Task.Run(() => GetCustomersByRegion(dbname, dClient)).GetAwaiter().GetResult();
                CustomersByRegion.ForEach(x => Debug.WriteLine(x.FirstName + " " + x.LastName + " " + x.Region));

                CreateItem(dbname, dClient);

                //UpdateMultipleAttributes(dbname, dClient);

                //DeleteItem(dbname, dClient);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async Task<List<Customer>> GetCustomerById(string dbname, AmazonDynamoDBClient dClient)
        {
            
            var customers = new List<Customer>();

            using (var dbContext = new DynamoDBContext(dClient))
            {               
                //QueryAsync provides many query options.
                //Since the primary key is a unique customer identifier, there will every only be one customer that meets this guid filter.  
                //But if the primary key was an environment or a redundant item...then you would get multiple.
                customers = await dbContext.QueryAsync<Customer>("108faf16-dee7-4bc1-89e9-4985b89b2f85", new DynamoDBOperationConfig { OverrideTableName = dbname, IndexName = "CustomerId-index" }).GetRemainingAsync();
            }

            return customers;
        }

        public static async Task<List<Customer>> GetCustomersByRegion(string dbname, AmazonDynamoDBClient dClient)
        {

            var customers = new List<Customer>();

            using (var dbContext = new DynamoDBContext(dClient))
            {
                //QueryAsync provides many query options.
                //Since the primary key is a unique customer identifier, there will every only be one customer that meets this guid filter.  
                //But if the primary key was an environment or a redundant item...then you would get multiple.
                customers = await dbContext.QueryAsync<Customer>("NY", new DynamoDBOperationConfig { OverrideTableName = dbname, IndexName = "Region-index" }).GetRemainingAsync();
            }

            return customers;
        }

        private static void CreateItem(string dbname, AmazonDynamoDBClient dClient)
        {
            var request = new PutItemRequest
            {
                TableName = dbname,
                Item = new Dictionary<string, AttributeValue>()
                {
                    {
                        "CustomerId", new AttributeValue { S = Guid.NewGuid().ToString() }
                        //"CustomerId", new AttributeValue { S = "1000" }
                    },
                    {
                        "FirstName", new AttributeValue { S = "Insert firstname" }
                    },
                    {
                        "LastName", new AttributeValue { S = "Inset lastname" }
                    },
                    {
                        "Region", new AttributeValue { S = "NY" }
                    }
                }
            };
            dClient.PutItemAsync(request);
        }

        private static void UpdateMultipleAttributes(string dbname, AmazonDynamoDBClient dClient)
        {
            var request = new UpdateItemRequest
            {
                Key = new Dictionary<string, AttributeValue>()
            {
                {
                    "CustomerId", new AttributeValue { S = "1000" }
                }
            },
                // Perform the following updates:
                // 1) Set a new value to Region attribute
                // 1) Add a new attribute named City
                // 2) Remove the LastName attribute
                ExpressionAttributeNames = new Dictionary<string, string>()
            {
                {"#R","Region"},
                {"#CT","City"},
                {"#L","LastName"}
            },
                ExpressionAttributeValues = new Dictionary<string, AttributeValue>()
            {
                {":region",new AttributeValue {
                     S = "MD"
                 }},
                {":new",new AttributeValue {
                     S = "Baltimore"
                 }}
            },

                UpdateExpression = "SET #R :region ADD #CT = :new REMOVE #L",

                TableName = dbname,
                ReturnValues = "ALL_NEW" // Give me all attributes of the updated item.
            };
            var response = dClient.UpdateItemAsync(request);

            // Check the response.
            var result = response.IsCompletedSuccessfully; 
            Debug.WriteLine("\nPrinting update status............");
            Debug.WriteLine(result);
        }

        private static void DeleteItem(string dbname, AmazonDynamoDBClient dClient)
        {
            var request = new DeleteItemRequest
            {
                TableName = dbname,
                Key = new Dictionary<string, AttributeValue>()
                {
                    {
                        "CustomerId", new AttributeValue { S = "1000" }
                    }
                }
            };

            var response = dClient.DeleteItemAsync(request);

            // Check the response.
            var result = response.IsCompletedSuccessfully;
            Debug.WriteLine("\nPrinting delete status............");
            Debug.WriteLine(result);
        }
    }
}
